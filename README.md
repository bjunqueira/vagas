Olá!
Seguem algumas instruções e curiosidades sobre o desenvolvimento.

A documentação dos endpoints das APIs pode ser acessada em http://localhost:9000

Sobre o desenvolvimento:
    Foi escolhida a linguagem de programação PHP na versão 5.6 por ser a linguagem que tenho mais fluência, entretanto estou sempre aberto a desenvolver em outras linguagens e plataformas, esse é o tipo de desafio que tenho prazer em realizar.

Framework utilizado foi o Zend Framework 2, por atender bem os requisitos para construção de APIs utilizando design pattern MVC.

Para o banco de dados foi utilizado o MySQL, é possível encontrar um arquivo dump da base de dados em APPLICATION_ROOT/db/dumpDB.sql, os outros arquivos neste mesmo diretório são apenas para registrar as etapas do desenvolvimento.

O código pode ser encontrado no repositório do Bitbucket
    https://bitbucket.org/bjunqueira/vagas/

Foram criados tickets 'issues' também no Bitbucket para vincular os commits a cada um dos issues e assim separar seus contextos
    https://bitbucket.org/bjunqueira/vagas/issues

Não foi possível criar testes unitários ou funcionais dentro do prazo estipulado, caso necessário, e se houver interesse em que eu os desenvolva farei com prazer.

Agradeço a atenção e me mantenho a disposição para quaisquer dúvidas.

-Bruno Junqueira

email: junqueira.bj@gmail.com
skype: bj.junqueira
cel: (19) 9 9204.0534