--
-- Issue #3: Criar endpoint para cadastrar vagas
--


-- Create table to store expertise levels.
	CREATE TABLE `recruiterdb`.`expertise` (
	  `id_expertise` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
	  `level` INT(1) NULL,
	  `title` VARCHAR(45) NULL COMMENT 'Table to store expertise level and title',
	  PRIMARY KEY (`id_expertise`))
	ENGINE = InnoDB;


-- Populate table.
	INSERT INTO `recruiterdb`.`expertise` (`level`, `title`) VALUES ('1', 'estagiário');
	INSERT INTO `recruiterdb`.`expertise` (`level`, `title`) VALUES ('2', 'júnior');
	INSERT INTO `recruiterdb`.`expertise` (`level`, `title`) VALUES ('3', 'pleno');
	INSERT INTO `recruiterdb`.`expertise` (`level`, `title`) VALUES ('4', 'sênior');
	INSERT INTO `recruiterdb`.`expertise` (`level`, `title`) VALUES ('5', 'especialista');


-- Create location table and populate it.
	CREATE TABLE `recruiterdb`.`location` (
	  `id_location` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
	  `name` VARCHAR(1) NULL,
	  PRIMARY KEY (`id_location`))
	ENGINE = InnoDB;

	INSERT INTO `recruiterdb`.`location` (`name`) VALUES ('A');
	INSERT INTO `recruiterdb`.`location` (`name`) VALUES ('B');
	INSERT INTO `recruiterdb`.`location` (`name`) VALUES ('C');
	INSERT INTO `recruiterdb`.`location` (`name`) VALUES ('D');
	INSERT INTO `recruiterdb`.`location` (`name`) VALUES ('E');
	INSERT INTO `recruiterdb`.`location` (`name`) VALUES ('F');



-- Create job_opportunity table.
    CREATE TABLE `recruiterdb`.`job_opportunity` (
      `id_job_opportunity` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
      `id_expertise` INT(11) UNSIGNED NULL,
      `id_location` INT(11) UNSIGNED NULL,
      `company` TEXT NULL,
      `title` TEXT NULL,
      `description` TEXT NULL,
      PRIMARY KEY (`id_job_opportunity`),
      INDEX `fk_job_opportunity_level_idx` (`id_expertise` ASC),
      INDEX `fk_ job_opportunity_location_idx` (`id_location` ASC),
      CONSTRAINT `fk_job_opportunity_expertise`
        FOREIGN KEY (`id_expertise`)
        REFERENCES `recruiterdb`.`expertise` (`id_expertise`)
        ON DELETE NO ACTION
        ON UPDATE NO ACTION,
      CONSTRAINT `fk_ job_opportunity_location`
        FOREIGN KEY (`id_location`)
        REFERENCES `recruiterdb`.`location` (`id_location`)
        ON DELETE NO ACTION
        ON UPDATE NO ACTION)
    ENGINE = InnoDB;
