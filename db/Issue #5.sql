--
-- Issue #5: Criar endpoint para vincular candidatos à vagas
--


-- Create table to store candidatures.

    CREATE TABLE `recruiterdb`.`candidature` (
      `id_candidature` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
      `id_candidate` INT(11) UNSIGNED NULL,
      `id_job_opportunity` INT(11) UNSIGNED NULL,
      `created_on` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP,
      PRIMARY KEY (`id_candidature`),
      INDEX `fk_candidature_candidate_idx` (`id_candidate` ASC),
      INDEX `fk_candidature_job_opportunity_idx` (`id_job_opportunity` ASC),
      CONSTRAINT `fk_candidature_candidate`
        FOREIGN KEY (`id_candidate`)
        REFERENCES `recruiterdb`.`candidate` (`id_candidate`)
        ON DELETE NO ACTION
        ON UPDATE NO ACTION,
      CONSTRAINT `fk_candidature_job_opportunity`
        FOREIGN KEY (`id_job_opportunity`)
        REFERENCES `recruiterdb`.`job_opportunity` (`id_job_opportunity`)
        ON DELETE NO ACTION
        ON UPDATE NO ACTION)
    ENGINE = InnoDB;


-- Add column score to candidature table.


    ALTER TABLE `recruiterdb`.`candidature` 
    ADD COLUMN `score` INT NULL AFTER `id_job_opportunity`;
