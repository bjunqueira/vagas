--
-- Issue #6: Criar um endpoint para retornar os candidatos de uma vaga, ordenados pelo score (de forma decrescente)
--


-- Create distance table.

    CREATE TABLE `recruiterdb`.`distance` (
      `id_distance` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
      `from` VARCHAR(1) NULL,
      `to` VARCHAR(1) NULL,
      `distance` INT NULL,
      PRIMARY KEY (`id_distance`))
    ENGINE = InnoDB;
    ALTER TABLE `recruiterdb`.`distance` 
    CHANGE COLUMN `from` `from` INT(11) UNSIGNED NULL DEFAULT NULL ,
    CHANGE COLUMN `to` `to` INT(11) UNSIGNED NULL DEFAULT NULL ,
    ADD INDEX `fk_distance_from_idx` (`from` ASC),
    ADD INDEX `fk_distance_to_idx` (`to` ASC);
    ALTER TABLE `recruiterdb`.`distance` 
    ADD CONSTRAINT `fk_distance_from`
      FOREIGN KEY (`from`)
      REFERENCES `recruiterdb`.`location` (`id_location`)
      ON DELETE NO ACTION
      ON UPDATE NO ACTION,
    ADD CONSTRAINT `fk_distance_to`
      FOREIGN KEY (`to`)
      REFERENCES `recruiterdb`.`location` (`id_location`)
      ON DELETE NO ACTION
      ON UPDATE NO ACTION;

-- Populate table.

    INSERT INTO `recruiterdb`.`distance` (`from`, `to`, `distance`) VALUES ('1', '2', '5');
    INSERT INTO `recruiterdb`.`distance` (`from`, `to`, `distance`) VALUES ('2', '1', '5');
    INSERT INTO `recruiterdb`.`distance` (`from`, `to`, `distance`) VALUES ('2', '3', '7');
    INSERT INTO `recruiterdb`.`distance` (`from`, `to`, `distance`) VALUES ('2', '4', '3');
    INSERT INTO `recruiterdb`.`distance` (`from`, `to`, `distance`) VALUES ('3', '2', '7');
    INSERT INTO `recruiterdb`.`distance` (`from`, `to`, `distance`) VALUES ('3', '5', '4');
    INSERT INTO `recruiterdb`.`distance` (`from`, `to`, `distance`) VALUES ('4', '2', '3');
    INSERT INTO `recruiterdb`.`distance` (`from`, `to`, `distance`) VALUES ('4', '5', '10');
    INSERT INTO `recruiterdb`.`distance` (`from`, `to`, `distance`) VALUES ('4', '6', '8');
    INSERT INTO `recruiterdb`.`distance` (`from`, `to`, `distance`) VALUES ('5', '3', '4');
    INSERT INTO `recruiterdb`.`distance` (`from`, `to`, `distance`) VALUES ('5', '4', '10');
    INSERT INTO `recruiterdb`.`distance` (`from`, `to`, `distance`) VALUES ('6', '4', '8');
