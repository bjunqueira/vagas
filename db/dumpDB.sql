-- MySQL dump 10.13  Distrib 5.5.50, for osx10.8 (i386)
--
-- Host: 127.0.0.1    Database: recruiterdb
-- ------------------------------------------------------
-- Server version	5.5.50

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Current Database: `recruiterdb`
--

CREATE DATABASE /*!32312 IF NOT EXISTS*/ `recruiterdb` /*!40100 DEFAULT CHARACTER SET utf8 COLLATE utf8_bin */;

USE `recruiterdb`;

--
-- Table structure for table `candidate`
--

DROP TABLE IF EXISTS `candidate`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `candidate` (
  `id_candidate` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `id_location` int(11) unsigned DEFAULT NULL,
  `id_expertise` int(11) unsigned DEFAULT NULL,
  `name` text COLLATE utf8_bin,
  `job_title` text COLLATE utf8_bin,
  PRIMARY KEY (`id_candidate`),
  KEY `fk_ candidate_location_idx` (`id_location`),
  KEY `fk_ candidate_expertise_idx` (`id_expertise`),
  CONSTRAINT `fk_ candidate_expertise` FOREIGN KEY (`id_expertise`) REFERENCES `expertise` (`id_expertise`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_ candidate_location` FOREIGN KEY (`id_location`) REFERENCES `location` (`id_location`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `candidate`
--

LOCK TABLES `candidate` WRITE;
/*!40000 ALTER TABLE `candidate` DISABLE KEYS */;
/*!40000 ALTER TABLE `candidate` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `candidature`
--

DROP TABLE IF EXISTS `candidature`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `candidature` (
  `id_candidature` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `id_candidate` int(11) unsigned DEFAULT NULL,
  `id_job_opportunity` int(11) unsigned DEFAULT NULL,
  `score` int(11) DEFAULT NULL,
  `created_on` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id_candidature`),
  KEY `fk_candidature_candidate_idx` (`id_candidate`),
  KEY `fk_candidature_job_opportunity_idx` (`id_job_opportunity`),
  CONSTRAINT `fk_candidature_candidate` FOREIGN KEY (`id_candidate`) REFERENCES `candidate` (`id_candidate`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_candidature_job_opportunity` FOREIGN KEY (`id_job_opportunity`) REFERENCES `job_opportunity` (`id_job_opportunity`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `candidature`
--

LOCK TABLES `candidature` WRITE;
/*!40000 ALTER TABLE `candidature` DISABLE KEYS */;
/*!40000 ALTER TABLE `candidature` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `distance`
--

DROP TABLE IF EXISTS `distance`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `distance` (
  `id_distance` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `from` int(11) unsigned DEFAULT NULL,
  `to` int(11) unsigned DEFAULT NULL,
  `distance` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_distance`),
  KEY `fk_distances_from_idx` (`from`),
  KEY `fk_distances_to_idx` (`to`),
  CONSTRAINT `fk_distances_from` FOREIGN KEY (`from`) REFERENCES `location` (`id_location`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_distances_to` FOREIGN KEY (`to`) REFERENCES `location` (`id_location`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `distance`
--

LOCK TABLES `distance` WRITE;
/*!40000 ALTER TABLE `distance` DISABLE KEYS */;
INSERT INTO `distance` VALUES (1,1,2,5),(2,2,1,5),(3,2,3,7),(4,2,4,3),(5,3,2,7),(6,3,5,4),(7,4,2,3),(8,4,5,10),(9,4,6,8),(10,5,3,4),(11,5,4,10),(12,6,4,8);
/*!40000 ALTER TABLE `distance` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `expertise`
--

DROP TABLE IF EXISTS `expertise`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `expertise` (
  `id_expertise` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `level` int(1) DEFAULT NULL,
  `title` varchar(45) COLLATE utf8_bin DEFAULT NULL COMMENT 'Table to store expertise level and title',
  PRIMARY KEY (`id_expertise`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `expertise`
--

LOCK TABLES `expertise` WRITE;
/*!40000 ALTER TABLE `expertise` DISABLE KEYS */;
INSERT INTO `expertise` VALUES (1,1,'estagiário'),(2,2,'júnior'),(3,3,'pleno'),(4,4,'sênior'),(5,5,'especialista');
/*!40000 ALTER TABLE `expertise` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `job_opportunity`
--

DROP TABLE IF EXISTS `job_opportunity`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `job_opportunity` (
  `id_job_opportunity` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `id_expertise` int(11) unsigned DEFAULT NULL,
  `id_location` int(11) unsigned DEFAULT NULL,
  `company` text COLLATE utf8_bin,
  `title` text COLLATE utf8_bin,
  `description` text COLLATE utf8_bin,
  PRIMARY KEY (`id_job_opportunity`),
  KEY `fk_job_opportunity_level_idx` (`id_expertise`),
  KEY `fk_ job_opportunity_location_idx` (`id_location`),
  CONSTRAINT `fk_ job_opportunity_location` FOREIGN KEY (`id_location`) REFERENCES `location` (`id_location`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_job_opportunity_expertise` FOREIGN KEY (`id_expertise`) REFERENCES `expertise` (`id_expertise`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `job_opportunity`
--

LOCK TABLES `job_opportunity` WRITE;
/*!40000 ALTER TABLE `job_opportunity` DISABLE KEYS */;
/*!40000 ALTER TABLE `job_opportunity` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `location`
--

DROP TABLE IF EXISTS `location`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `location` (
  `id_location` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(1) COLLATE utf8_bin DEFAULT NULL,
  PRIMARY KEY (`id_location`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `location`
--

LOCK TABLES `location` WRITE;
/*!40000 ALTER TABLE `location` DISABLE KEYS */;
INSERT INTO `location` VALUES (1,'A'),(2,'B'),(3,'C'),(4,'D'),(5,'E'),(6,'F');
/*!40000 ALTER TABLE `location` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-01-14 20:09:31
