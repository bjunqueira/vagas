--
-- Issue #2: Configurar DB
--

-- Create DB
CREATE DATABASE recruiterdb CHARACTER SET utf8 COLLATE utf8_bin;

-- Create User
CREATE USER 'u_recruiter'@'127.0.0.1' IDENTIFIED BY 'q1RXwuCV1HSBJ4cXgrtyRsvmwya67ZV';

-- Grant permission to User
GRANT ALL ON recruiterdb.* TO 'u_recruiter'@'127.0.0.1';
GRANT ALL ON recruiterdb.* TO 'u_recruiter'@'localhost';