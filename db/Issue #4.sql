--
-- Issue #4: Criar endpoint para cadastrar candidatos
--


-- Create table to store candidates.

    CREATE TABLE `recruiterdb`.`candidate` (
      `id_candidate` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
      `id_location` INT(11) UNSIGNED NULL,
      `id_expertise` INT(11) UNSIGNED NULL,
      `name` TEXT NULL,
      `job_title` TEXT NULL,
      PRIMARY KEY (`id_candidate`),
      INDEX `fk_ candidate_location_idx` (`id_location` ASC),
      INDEX `fk_ candidate_expertise_idx` (`id_expertise` ASC),
      CONSTRAINT `fk_ candidate_location`
        FOREIGN KEY (`id_location`)
        REFERENCES `recruiterdb`.`location` (`id_location`)
        ON DELETE NO ACTION
        ON UPDATE NO ACTION,
      CONSTRAINT `fk_ candidate_expertise`
        FOREIGN KEY (`id_expertise`)
        REFERENCES `recruiterdb`.`expertise` (`id_expertise`)
        ON DELETE NO ACTION
        ON UPDATE NO ACTION)
    ENGINE = InnoDB;



