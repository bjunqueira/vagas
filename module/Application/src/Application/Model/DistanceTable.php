<?php
namespace Application\Model;


/**
 * @author brunojunqueira
 */
class DistanceTable extends BaseTable
{


    /**
     * Local cache.
     * @var array
     */
    private $_distancesGraph = array();


    /**
     * Calculates the shortest path between two locations. (Dijkstra's algorithm)
     *
     * @param int $from Location id of starting point.
     * @param int $to   Location if of ending point.
     *
     * @return int The distance.
     */
    public function shortestPath($from, $to)
    {
        $distancesGraph = $this->_getDistancesGraph();

        // Initialize arrays for storing data during calculation.
        $theNearestPath      = array();
        $leftNodesNotNearest = array();

        foreach(array_keys($distancesGraph) as $key) {
            $leftNodesNotNearest[$key] = 999999; // Infinite.
        }

        $leftNodesNotNearest[$from] = 0;

        // Start calculating.
        while(!empty($leftNodesNotNearest)) {
            $min = array_search(min($leftNodesNotNearest), $leftNodesNotNearest); // Find the min weight.
            if($min == $to) {
                break;
            }
            foreach($distancesGraph[$min] as $key => $val) {
                if (!empty($leftNodesNotNearest[$key])
                    && $leftNodesNotNearest[$min] + $val < $leftNodesNotNearest[$key]
                ) {
                    $leftNodesNotNearest[$key] = $leftNodesNotNearest[$min] + $val;
                    $theNearestPath[$key]      = array($min, $leftNodesNotNearest[$key]);
                }
            }
            unset($leftNodesNotNearest[$min]);
        }

        // Calculate distance.
        $pos      = $to;
        $distance = 0;
        while($pos != $from) {
            $previousPoint = $pos;
            $pos           = $theNearestPath[$pos][0];
            $distance     += $distancesGraph[$previousPoint][$pos];
        }

        return $distance;

    }//end shortestPath()


    /**
     * Build a distance graph to be used in the distance calculation.
     *
     * @return array
     */
    private function _getDistancesGraph()
    {
        // Build distances graph if not built yet.
        if (empty($this->_distancesGraph)) {
            $Distances = $this->getList();
            foreach ($Distances as $Distance) {
                $this->_distancesGraph[$Distance->from][$Distance->to] = $Distance->distance;
            }
        }

        return $this->_distancesGraph;

    }//end _getDistancesGraph()


}//end class