<?php
namespace Application\Model;


/**
 * @author brunojunqueira
 */
class CandidateTable extends BaseTable
{


    /**
     * Insert or update candidate.
     *
     * @param Candidate $Candidate Candidate to be created or updated.
     *
     * @throws \Exception
     */
    public function save(Candidate $Candidate)
    {
        $data = $Candidate->toArray();
        $id   = $Candidate->id_candidate;

        if (!$id) {
            $this->TableGateway->insert($data);
            return $this->TableGateway->lastInsertValue;
        } else {
            if ($this->get(array('id_candidate' => $id))) {
                $this->TableGateway->update($data, array('id_candidate' => $id));
                return $id;
            } else {
                throw new \Exception('Candidate id does not exist');
            }
        }

    }//end save();


}//end class