<?php
namespace Application\Model;


/**
 * @author brunojunqueira
 */
class Distance extends Base
{


    /**
     * @var int
     */
    public $id_distance;


    /**
     * @var int
     */
    public $from;


    /**
     * @var int
     */
    public $to;


    /**
     * @var int
     */
    public $distance;


    /**
     * In order to work with Zend\Db’s TableGateway class, we need to implement the exchangeArray() method.
     *
     * @param array $data Object data.
     *
     * @return void
     */
    public function exchangeArray($data)
    {
        $this->id_distance = (!empty($data['id_location'])) ? $data['id_location'] : null;
        $this->from        = (!empty($data['from'])) ? $data['from'] : null;
        $this->to          = (!empty($data['to'])) ? $data['to'] : null;
        $this->distance    = (!empty($data['distance'])) ? $data['distance'] : null;

    }//end exchangeArray()


}//end class