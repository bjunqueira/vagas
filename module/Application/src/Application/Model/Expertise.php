<?php
namespace Application\Model;


/**
 * @author brunojunqueira
 */
class Expertise extends Base
{


    /**
     * @var int
     */
    public $id_expertise;


    /**
     * @var int
     */
    public $level;


    /**
     * @var string
     */
    public $title;


    /**
     * In order to work with Zend\Db’s TableGateway class, we need to implement the exchangeArray() method.
     *
     * @param array $data Object data.
     *
     * @return void
     */
    public function exchangeArray($data)
    {
        $this->id_expertise = (!empty($data['id_expertise'])) ? $data['id_expertise'] : null;
        $this->level        = (!empty($data['level'])) ? $data['level'] : null;
        $this->title        = (!empty($data['title'])) ? $data['title'] : null;

    }//end exchangeArray()


}//end class