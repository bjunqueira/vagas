<?php
namespace Application\Model;

use Zend\Db\TableGateway\TableGateway;

/**
 * @author brunojunqueira
 */
class BaseTable
{


    /**
     * @var \Zend\Db\TableGateway\TableGateway
     */
    protected $TableGateway;


    /**
     * The constructor.
     *
     * @param TableGateway $TableGateway Table gateway object.
     */
    public function __construct(TableGateway $TableGateway)
    {
        $this->TableGateway = $TableGateway;

    }//end __construct()


    /**
     * Get entity from DB filtering entries by $filters.
     *
     * @param array $filters Query filters.
     *
     * @return mixed
     */
    public function get(array $filters = array())
    {
        $rowset = $this->TableGateway->select($filters);
        $row    = $rowset->current();

        return $row;

    }//end get()


    /**
     * Get entities from DB filtering entries by $filters.
     *
     * @param array $filters Query filters.
     */
    public function getList(array $filters = array())
    {
        return $this->TableGateway->select($filters);

    }//end getList()


}//end class