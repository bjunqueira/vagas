<?php
namespace Application\Model;


/**
 * @author brunojunqueira
 */
class JobOpportunityTable extends BaseTable
{


    /**
     * Insert or update job opportunity.
     *
     * @param JobOpportunity $JobOpportunity Job opportunity to be created or updated.
     *
     * @throws \Exception
     */
    public function save(JobOpportunity $JobOpportunity)
    {
        $data = $JobOpportunity->toArray();
        $id   = $JobOpportunity->id_job_opportunity;

        if (!$id) {
            $this->TableGateway->insert($data);
            return $this->TableGateway->lastInsertValue;
        } else {
            if ($this->get(array('id_job_opportunity' => $id))) {
                $this->TableGateway->update($data, array('id_job_opportunity' => $id));
                return $id;
            } else {
                throw new \Exception('Job opportunity id does not exist');
            }
        }

    }//end save();


    /**
     * Fetch all job opportunities.
     *
     * @param array $filters Query filters.
     *
     * @return \Zend\Db\ResultSet\ResultSet
     */
    public function fetchAll(array $filters = array())
    {
        $resultSet = $this->TableGateway->select($filters);
        return $resultSet;

    }//end fetchAll()


}//end class