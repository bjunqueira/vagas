<?php
namespace Application\Model;

/**
 * @author brunojunqueira
 */
class Base
{


    /**
     * Convert object into array.
     *
     * @return array
     */
    public function toArray()
    {
        $array = array();
        foreach (array_keys((array) $this) as $attribute) {
            if (empty($this->$attribute) or is_object($this->$attribute)) {
                continue;
            }
            $array[$attribute] = $this->$attribute;
        }
        return $array;

    }//end toArray()


}//end class