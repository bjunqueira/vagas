<?php
namespace Application\Model;


use Zend\Db\Sql\Select;

/**
 * @author brunojunqueira
 */
class CandidatureTable extends BaseTable
{


    /**
     * Insert or update candidature.
     *
     * @param Candidature $Candidature Candidature to be created or updated.
     *
     * @throws \Exception
     */
    public function save(Candidature $Candidature)
    {
        $data = $Candidature->toArray();
        $id   = $Candidature->id_candidature;

        if (!$id) {
            $this->TableGateway->insert($data);
            return $this->TableGateway->lastInsertValue;
        } else {
            if ($this->get(array('id_candidature' => $id))) {
                $this->TableGateway->update($data, array('id_candidature' => $id));
                return $id;
            } else {
                throw new \Exception('Candidature id does not exist');
            }
        }

    }//end save();


    /**
     * Get ranking of candidates for a given job opportunity.
     *
     * @param int $idJobOpportunity Job opportunity id.
     *
     * @return array
     */
    public function getRanking($idJobOpportunity)
    {
        $Select = $this->TableGateway->getSql()->select();
        $Select->join('candidate', 'candidate.id_candidate = candidature.id_candidate', array('nome' => 'name', 'profissao' => 'job_title'));
        $Select->join('location', 'candidate.id_location = location.id_location', array('localizacao' => 'name'));
        $Select->join('expertise', 'expertise.id_expertise = candidate.id_expertise', array('nivel' => 'level'));
        $Select->columns(array('score'));
        $Select->where('candidature.id_job_opportunity = ' . $idJobOpportunity);
        $Select->order('candidature.score DESC');

        $statement = $this->TableGateway->getSql()->prepareStatementForSqlObject($Select);
        $resultSet = $statement->execute();
        $ranking = array();
        foreach ($resultSet as $row) {
            $ranking[] = $row;
        }
        return $ranking;

    }//end getRanking()


}//end class