<?php
namespace Application\Model;


/**
 * @author brunojunqueira
 */
class Location extends Base
{


    /**
     * @var int
     */
    public $id_location;


    /**
     * @var string
     */
    public $name;


    /**
     * In order to work with Zend\Db’s TableGateway class, we need to implement the exchangeArray() method.
     *
     * @param array $data Object data.
     *
     * @return void
     */
    public function exchangeArray($data)
    {
        $this->id_location = (!empty($data['id_location'])) ? $data['id_location'] : null;
        $this->name        = (!empty($data['name'])) ? $data['name'] : null;

    }//end exchangeArray()


}//end class