<?php
namespace Application\Model;


/**
 * Model to deal with candidatures.
 *
 * @author brunojunqueira
 */
class Candidature extends Base
{


    /**
     * @var int
     */
    public $id_candidature;


    /**
     * @var int
     */
    public $id_candidate;


    /**
     * @var int
     */
    public $id_job_opportunity;


    /**
     * @var int
     */
    public $score;


    /**
     * @var Candidate
     */
    private $_Candidate;


    /**
     * @var JobOpportunity
     */
    private $_JobOpportunity;


    /**
     * @var DistanceTable
     */
    private $_DistanceTable;


    /**
     * The constructor.
     *
     * @param DistanceTable $DistanceTable Distance table service.
     *
     * @return void
     */
    public function __construct(DistanceTable $DistanceTable)
    {
        $this->_DistanceTable = $DistanceTable;

    }//end __construct()


    /**
     * Setter.
     *
     * @param Candidate $Candidate The candidate object.
     *
     * @return void
     */
    public function setCandidate(Candidate $Candidate)
    {
        $this->id_candidate = $Candidate->id_candidate;
        $this->_Candidate   = $Candidate;

        if (!is_null($this->_JobOpportunity)) {
            $this->_updateScore();
        }

    }//end setCandidate()


    /**
     * Setter.
     *
     * @param JobOpportunity $JobOpportunity The job opportunity object.
     *
     * @return void
     */
    public function setJobOpportunity(JobOpportunity $JobOpportunity)
    {
        $this->id_job_opportunity = $JobOpportunity->id_job_opportunity;
        $this->_JobOpportunity    = $JobOpportunity;

        if (!is_null($this->_Candidate)) {
            $this->_updateScore();
        }

    }//end setJobOpportunity()


    /**
     * Update score whenever JobOpportunity and Candidate are set.
     *
     * @return void
     */
    private function _updateScore()
    {
        // Calculate expertise.
        $expertiseJobOpportunity = $this->_JobOpportunity->getExpertise()->level;
        $expertiseCandidate      = $this->_Candidate->getExpertise()->level;

        $expertise = 100 - 25 * abs($expertiseJobOpportunity - $expertiseCandidate);

        // Calculate location.
        $locationJobOpportunity = $this->_JobOpportunity->id_location;
        $locationCandidate      = $this->_Candidate->id_location;

        $distance = $this->_DistanceTable->shortestPath($locationJobOpportunity, $locationCandidate);
        if ($distance <= 5) {
            $distance = 100;

        } else if ($distance <= 10) {
            $distance = 75;

        } else if ($distance <= 15) {
            $distance = 50;

        } else if ($distance <= 20) {
            $distance = 25;

        } else {
            $distance = 0;
        }

        // Set the score. Floor to discard the decimal part of the score.
        $this->score = floor(($expertise + $distance)/2); 

    }//end _updateScore()


    /**
     * In order to work with Zend\Db’s TableGateway class, we need to implement the exchangeArray() method.
     *
     * @param array $data Object data.
     *
     * @return void
     */
    public function exchangeArray($data)
    {
        $this->id_candidature     = (!empty($data['id_candidature'])) ? $data['id_candidature'] : null;
        $this->id_candidate       = (!empty($data['id_candidate'])) ? $data['id_candidate'] : null;
        $this->id_job_opportunity = (!empty($data['id_job_opportunity'])) ? $data['id_job_opportunity'] : null;
        $this->score              = (!empty($data['score'])) ? $data['score'] : null;

    }//end exchangeArray()


}//end class