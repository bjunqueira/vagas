<?php
namespace Application\Model;


/**
 * Model to keep candidate logic.
 *
 * @author brunojunqueira
 */
class Candidate extends Base
{


    /**
     * @var int
     */
    public $id_candidate;


    /**
     * @var int
     */
    public $id_expertise;


    /**
     * @var int
     */
    public $id_location;


    /**
     * @var string
     */
    public $name;


    /**
     * @var string
     */
    public $job_title;


    /**
     * @var Location
     */
    private $_Location;


    /**
     * @var Expertise
     */
    private $_Expertise;


    /**
     * @var LocationTable
     */
    private $_LocationTable;


    /**
     * @var ExpertiseTable
     */
    private $_ExpertiseTable;


    /**
     * Setter.
     *
     * @param Location $Location The location object.
     *
     * @return void
     */
    public function setLocation(Location $Location)
    {
        $this->id_location = $Location->id_location;
        $this->_Location   = $Location;

    }//end setLocation()


    /**
     * Setter.
     *
     * @param LocationTable $LocationTable The location table.
     *
     * @return void
     */
    public function setLocationTable(LocationTable $LocationTable)
    {
        $this->_LocationTable = $LocationTable;

    }//end setLocationTable()


    /**
     * Setter.
     *
     * @param ExpertiseTable $ExpertiseTable The expertise table.
     *
     * @return void
     */
    public function setExpertiseTable(ExpertiseTable $ExpertiseTable)
    {
        $this->_ExpertiseTable = $ExpertiseTable;

    }//end setExpertiseTable()


    /**
     * Getter.
     *
     * @return \Application\Model\Expertise
     */
    public function getExpertise()
    {
        // Loads if necessary.
        if (is_null($this->_Expertise)) {
            if (!empty($this->id_expertise)) {
                $this->_Expertise = $this->_ExpertiseTable->get(array('id_expertise' => $this->id_expertise));
            } else {
                throw new \Exception('Cannot load Expertise');
            }
        }
        return $this->_Expertise;

    }//end getExpertise()


    /**
     * Getter.
     *
     * @return \Application\Model\Location
     */
    public function getLocation()
    {
        // Loads if necessary.
        if (is_null($this->_Location)) {
            if (!empty($this->id_location)) {
                $this->_Location = $this->_LocationTable->get(array('id_location' => $this->id_location));
            } else {
                throw new \Exception('Cannot load Location');
            }
        }
        return $this->_Location;

    }//end getLocation()


    /**
     * Setter.
     *
     * @param Expertise $Expertise The expertise object.
     *
     * @return void
     */
    public function setExpertise(Expertise $Expertise)
    {
        $this->id_expertise = $Expertise->id_expertise;
        $this->_Expertise   = $Expertise;

    }//end setExpertise()


    /**
     * In order to work with Zend\Db’s TableGateway class, we need to implement the exchangeArray() method.
     *
     * @param array $data Object data.
     *
     * @return void
     */
    public function exchangeArray($data)
    {
        $this->id_candidate = (!empty($data['id_candidate'])) ? $data['id_candidate'] : null;
        $this->id_location  = (!empty($data['id_location'])) ? $data['id_location'] : null;
        $this->id_expertise = (!empty($data['id_expertise'])) ? $data['id_expertise'] : null;
        $this->name         = (!empty($data['name'])) ? $data['name'] : null;
        $this->job_title    = (!empty($data['job_title'])) ? $data['job_title'] : null;

    }//end exchangeArray()


}//end class