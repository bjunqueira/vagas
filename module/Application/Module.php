<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2015 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Application;

use Zend\Mvc\ModuleRouteListener;
use Zend\Mvc\MvcEvent;
use Zend\Db\ResultSet\ResultSet;
use Application\Model\JobOpportunity;
use Zend\Db\TableGateway\TableGateway;
use Application\Model\JobOpportunityTable;
use Application\Model\LocationTable;
use Application\Model\ExpertiseTable;
use Application\Model\Location;
use Application\Model\Expertise;
use Application\Model\CandidateTable;
use Application\Model\Candidate;
use Application\Model\CandidatureTable;
use Application\Model\Candidature;
use Application\Model\Distance;
use Application\Model\DistanceTable;

class Module
{
    public function onBootstrap(MvcEvent $e)
    {
        $eventManager        = $e->getApplication()->getEventManager();
        $moduleRouteListener = new ModuleRouteListener();
        $moduleRouteListener->attach($eventManager);
    }

    public function getConfig()
    {
        return include __DIR__ . '/config/module.config.php';
    }

    public function getAutoloaderConfig()
    {
        return array(
            'Zend\Loader\StandardAutoloader' => array(
                'namespaces' => array(
                    __NAMESPACE__ => __DIR__ . '/src/' . __NAMESPACE__,
                ),
            ),
        );
    }

    public function getServiceConfig()
    {
        return array(
            'factories' => array(
                'Application\Model\JobOpportunityTable' => function($sm) {
                    $tableGateway = $sm->get('JobOpportunityTableGateway');
                    $table = new JobOpportunityTable($tableGateway);
                    return $table;
                },
                'JobOpportunityTableGateway' => function ($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $resultSetPrototype = new ResultSet();
                    $LocationTable  = $sm->get('Application\Model\LocationTable');
                    $ExpertiseTable = $sm->get('Application\Model\ExpertiseTable');
                    $JobOpportunity = new JobOpportunity();
                    $JobOpportunity->setExpertiseTable($ExpertiseTable);
                    $JobOpportunity->setLocationTable($LocationTable);
                    $resultSetPrototype->setArrayObjectPrototype($JobOpportunity);
                    return new TableGateway('job_opportunity', $dbAdapter, null, $resultSetPrototype);
                },
                'Application\Model\LocationTable' => function($sm) {
                    $tableGateway = $sm->get('LocationTableGateway');
                    $table = new LocationTable($tableGateway);
                    return $table;
                },
                'LocationTableGateway' => function ($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new Location());
                    return new TableGateway('location', $dbAdapter, null, $resultSetPrototype);
                },
                'Application\Model\ExpertiseTable' => function($sm) {
                    $tableGateway = $sm->get('ExpertiseTableGateway');
                    $table = new ExpertiseTable($tableGateway);
                    return $table;
                },
                'ExpertiseTableGateway' => function ($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new Expertise());
                    return new TableGateway('expertise', $dbAdapter, null, $resultSetPrototype);
                },
                'Application\Model\CandidateTable' => function($sm) {
                    $tableGateway = $sm->get('CandidateTableGateway');
                    $table = new CandidateTable($tableGateway);
                    return $table;
                },
                'CandidateTableGateway' => function ($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $resultSetPrototype = new ResultSet();
                    $LocationTable  = $sm->get('Application\Model\LocationTable');
                    $ExpertiseTable = $sm->get('Application\Model\ExpertiseTable');
                    $Candidate = new Candidate();
                    $Candidate->setExpertiseTable($ExpertiseTable);
                    $Candidate->setLocationTable($LocationTable);
                    $resultSetPrototype->setArrayObjectPrototype($Candidate);
                    return new TableGateway('candidate', $dbAdapter, null, $resultSetPrototype);
                },
                'Application\Model\CandidatureTable' => function($sm) {
                    $tableGateway = $sm->get('CandidatureTableGateway');
                    $table = new CandidatureTable($tableGateway);
                    return $table;
                },
                'CandidatureTableGateway' => function ($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $resultSetPrototype = new ResultSet();
                    $DistanceTable = $sm->get('Application\Model\DistanceTable');
                    $resultSetPrototype->setArrayObjectPrototype(new Candidature($DistanceTable));
                    return new TableGateway('candidature', $dbAdapter, null, $resultSetPrototype);
                },
                'Application\Model\DistanceTable' => function($sm) {
                    $tableGateway = $sm->get('DistanceTableGateway');
                    $table = new DistanceTable($tableGateway);
                    return $table;
                },
                'DistanceTableGateway' => function ($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new Distance());
                    return new TableGateway('distance', $dbAdapter, null, $resultSetPrototype);
                },
            ),
        );
    }

}//end class
