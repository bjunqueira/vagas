<?php

namespace AppApi;

use Zend\Mvc\ModuleRouteListener;
use Zend\Mvc\MvcEvent;

class Module
{


    public function onBootstrap(MvcEvent $e)
    {
        $eventManager        = $e->getApplication()->getEventManager();
        $moduleRouteListener = new ModuleRouteListener();
        $moduleRouteListener->attach($eventManager);

    }//end onBootstrap()


    public function getConfig()
    {
        return include __DIR__ . '/config/module.config.php';

    }//end getConfig()


    public function getAutoloaderConfig()
    {
        return array(
            'Zend\Loader\StandardAutoloader' => array(
                'namespaces' => array(
                    __NAMESPACE__ => __DIR__ . '/src/' . __NAMESPACE__,
                ),
            ),
        );

    }//end getAutoloaderConfig()


    /**
     * Factory for services to be used only in the API module.
     *
     * @return array
     */
    public function getServiceConfig()
    {
        return array(
            'factories' => array(),
        );

    }//end getServiceConfig()


}//end class