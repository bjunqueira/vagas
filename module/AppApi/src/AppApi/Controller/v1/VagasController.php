<?php
namespace AppApi\Controller\v1;


use AppApi\Controller\BaseController;
use Zend\Stdlib\ResponseInterface;
use Application\Model\JobOpportunity;

/**
 * Endpoints to handle job opportunities.
 *
 * @author brunojunqueira
 */
class VagasController extends BaseController
{


    /**
     * Endpoints base url.
     * @var string
     */
    const BASE_URL = '/v1/vagas';


    /**
     * Endpoint to create a new job opportunity.
     *
     * @param array $data Job opportunity data.
     * [
     *     'empresa'     => 'Test',                                     // Company name.
     *     'titulo'      => 'Vaga teste',                               // Job opportunity title.
     *     'descricao'   => 'Criar os mais diferentes tipos de teste',  // Job opportunity description.
     *     'localizacao' => 'A',                                        // Job opportunity location.
     *     'nivel'       => 3,                                          // Job opportunity candidate's expertise level.
     * ]
     *
     * @return ResponseInterface
     *
     * {@inheritDoc}
     * @see \Zend\Mvc\Controller\AbstractRestfulController::create()
     */
    public function create($data)
    {
        // Validate if received any data at all.
        if (empty($data)) {
            return $this->sendErrorResponse(400, 40001, 'Erro ao tentar criar vaga. Payload nao pode ser vazio.');
        }

        $JobOpportunity = new JobOpportunity();

        // Validate empresa param.
        if (isset($data['empresa'])) {
            $JobOpportunity->company = $data['empresa'];
        } else {
            return $this->sendErrorResponse(400, 40002, "Erro ao tentar criar vaga. Campo 'empresa' obrigatorio");
        }

        // Validates titulo param.
        if (isset($data['titulo'])) {
            $JobOpportunity->title = $data['titulo'];
        } else {
            return $this->sendErrorResponse(400, 40003, "Erro ao tentar criar vaga. Campo 'titulo' obrigatorio");
        }

        // Validate descricao param.
        if (isset($data['descricao'])) {
            $JobOpportunity->description = $data['descricao'];
        } else {
            return $this->sendErrorResponse(400, 40004, "Erro ao tentar criar vaga. Campo 'descricao' obrigatorio");
        }

        // Validate localizacao param.
        if (isset($data['localizacao'])) {
            $LocationTable = $this->getServiceLocator()->get('Application\Model\LocationTable');
            $Location      = $LocationTable->get(array('name' => $data['localizacao']));
            if (!empty($Location) and $Location->id_location) {
                $JobOpportunity->setLocation($Location);
            } else {
                return $this->sendErrorResponse(400, 40005, "Erro ao tentar criar vaga. Localizacao inexistente: {$data['localizacao']}");
            }
        } else {
            return $this->sendErrorResponse(400, 40006, "Erro ao tentar criar vaga. Campo 'localizacao' obrigatorio");
        }

        // Validate nivel param.
        if (isset($data['nivel'])) {
            $ExpertiseTable = $this->getServiceLocator()->get('Application\Model\ExpertiseTable');
            $Expertise      = $ExpertiseTable->get(array('level' => $data['nivel']));
            if (!empty($Expertise) and $Expertise->id_expertise) {
                $JobOpportunity->setExpertise($Expertise);
            } else {
                return $this->sendErrorResponse(400, 40007, "Erro ao tentar criar vaga. Nivel inexistente: {$data['nivel']}");
            }
        } else {
            return $this->sendErrorResponse(400, 40008, "Erro ao tentar criar vaga. Campo 'nivel' obrigatorio");
        }

        // Save new job opportunity.
        $JobOpportunityTable = $this->getServiceLocator()->get('Application\Model\JobOpportunityTable');
        $jobOpportunityId    = $JobOpportunityTable->save($JobOpportunity);

        // Build and return success response.
        $this->response->setStatusCode(201); // Created.
        $Response = $this->getResponse()
            ->setContent(
                json_encode(
                    array(
                        'code'    => 201,
                        'message' => 'Vaga criada com sucesso',
                        'id'      => $jobOpportunityId,
                        'href'    => sprintf('%s/%s', self::BASE_URL, $jobOpportunityId),
                    )
                )
            );
        return $Response;

    }//end create()


    /**
     * Get a ranking list of candidates for a given job opportunity.
     *
     * @return ResponseInterface
     */
    public function getRankingAction()
    {
        // Check whether the id_vaga param was received from route (url).
        $idVaga = $this->getEvent()->getRouteMatch()->getParam('id_vaga');
        if (empty($idVaga)) {
            return $this->sendErrorResponse(400, 40001, "Erro ao tentar pegar o ranking para vaga. Parametro 'id_vaga' obrigatorio.");
        }

        // Validates if there is a job opportunity with the given id.
        $JobOpportunityTable = $this->getServiceLocator()->get('Application\Model\JobOpportunityTable');
        $JobOpportunity      = $JobOpportunityTable->get(array('id_job_opportunity' => $idVaga));
        if (empty($JobOpportunity) or !$JobOpportunity->id_job_opportunity) {
            return $this->sendErrorResponse(400, 40002, "Erro ao tentar pegar o ranking para vaga. Vaga nao encontrada com id: {$idVaga}");
        }

        // Fetch all candidatures for the given job opportunity.
        $CandidatureTable = $this->getServiceLocator()->get('Application\Model\CandidatureTable');
        $ranking          = $CandidatureTable->getRanking($idVaga);

        // Build and send success response.
        $this->response->setStatusCode(200);
        $Response = $this->getResponse()
            ->setContent(json_encode($ranking)
        );
        return $Response;

    }//end getRankingAction()


}//end class