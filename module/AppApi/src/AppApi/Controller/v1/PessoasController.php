<?php
namespace AppApi\Controller\v1;


use AppApi\Controller\BaseController;
use Zend\Stdlib\ResponseInterface;
use Application\Model\Candidate;

/**
 * Endpoint to create a new candidate.
 *
 * @author brunojunqueira
 */
class PessoasController extends BaseController
{


    /**
     * Endpoints base url.
     * @var string
     */
    const BASE_URL = '/v1/pessoas';


    /**
     * Endpoint to create a new candidate.
     *
     * @param array $data Candidate data.
     * [
     *     'nome'        => 'John Doe',               // Candidate name.
     *     'profissao'   => 'Engenheiro de Software', // Candidate job's title.
     *     'localizacao' => 'C',                      // Candidate location.
     *     'nivel'       => 2,                        // Candidate expertise level.
     * ]
     *
     * @return ResponseInterface
     *
     * {@inheritDoc}
     * @see \Zend\Mvc\Controller\AbstractRestfulController::create()
     */
    public function create($data)
    {
        // Validate if received any data at all.
        if (empty($data)) {
            return $this->sendErrorResponse(400, 40001, 'Erro ao tentar criar candidato. Payload não pode ser vazio.');
        }

        $Candidate = new Candidate();

        // Validate name param.
        if (isset($data['nome'])) {
            $Candidate->name = $data['nome'];
        } else {
            return $this->sendErrorResponse(400, 40002, "Erro ao tentar criar candidato. Atributo 'nome' obrigatorio.");
        }

        // Validate profissao param.
        if (isset($data['profissao'])) {
            $Candidate->job_title = $data['profissao'];
        } else {
            return $this->sendErrorResponse(400, 40003, "Erro ao tentar criar candidato. Atributo 'profissao' obrigatorio.");
        }

        // Validade localizacao param.
        if (isset($data['localizacao'])) {
            $LocationTable = $this->getServiceLocator()->get('Application\Model\LocationTable');
            $Location      = $LocationTable->get(array('name' => $data['localizacao']));
            // Given location exists?
            if (!empty($Location) and $Location->id_location) {
                $Candidate->setLocation($Location);
            } else {
                return $this->sendErrorResponse(400, 40004, "Erro ao tentar criar candidato. Localizacao inexistente: {$data['localizacao']}");
            }
        } else {
            return $this->sendErrorResponse(400, 40005, "Erro ao tentar criar candidato. Atributo 'localizacao' obrigatorio.");
        }

        // Validade nivel param.
        if (isset($data['nivel'])) {
            $ExpertiseTable = $this->getServiceLocator()->get('Application\Model\ExpertiseTable');
            $Expertise      = $ExpertiseTable->get(array('level' => $data['nivel']));
            // Given level exists?
            if (!empty($Expertise) and $Expertise->id_expertise) {
                $Candidate->setExpertise($Expertise);
            } else {
                return $this->sendErrorResponse(400, 40006, "Erro ao tentar criar candidato. Nivel inexistente: {$data['nivel']}");
            }
        } else {
            return $this->sendErrorResponse(400, 40007, "Erro ao tentar criar candidato. Campo 'nivel' obrigatorio");
        }

        $CandidateTable = $this->getServiceLocator()->get('Application\Model\CandidateTable');
        $candidateId    = $CandidateTable->save($Candidate);

        // Build and return success response.
        $this->response->setStatusCode(201); // Created.
        $Response = $this->getResponse()
            ->setContent(
                json_encode(
                    array(
                        'code'    => 201,
                        'message' => 'Candidato criado com sucesso.',
                        'id'      => $candidateId,
                        'href'    => sprintf('%s/%s', self::BASE_URL, $candidateId),
                    )
                )
            );
        return $Response;

    }//end create()


}//end class