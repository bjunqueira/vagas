<?php
namespace AppApi\Controller\v1;


use AppApi\Controller\BaseController;
use Zend\Stdlib\ResponseInterface;
use Application\Model\Candidature;

/**
 * Endpoint to handle candidatures.
 *
 * @author brunojunqueira
 */
class CandidaturasController extends BaseController
{


    /**
     * Endpoints base url.
     * @var string
     */
    const BASE_URL = '/v1/candidaturas';


    /**
     * Endpoint to create a new candidate.
     *
     * @param array $data Candidate data.
     * [
     *     'id_vaga'   => 1, // Job opportunity id.
     *     'id_pessoa' => 2, // Candidate id.
     * ]
     *
     * @return ResponseInterface
     *
     * {@inheritDoc}
     * @see \Zend\Mvc\Controller\AbstractRestfulController::create()
     */
    public function create($data)
    {
        // Validate if received any data at all.
        if (empty($data)) {
            return $this->sendErrorResponse(400, 40001, 'Erro ao tentar criar uma candidatura. Payload nao pode ser vazio.');
        }

        $DistanceTable = $this->getServiceLocator()->get('Application\Model\DistanceTable');
        $Candidature   = new Candidature($DistanceTable);

        // Validates candidate.
        if (isset($data['id_pessoa'])) {
            $CandidateTable = $this->getServiceLocator()->get('Application\Model\CandidateTable');
            $Candidate      = $CandidateTable->get(array('id_candidate' => $data['id_pessoa']));
            if (!empty($Candidate) and $Candidate->id_candidate) {
                $Candidature->setCandidate($Candidate);
            } else {
                return $this->sendErrorResponse(400, 40002, "Erro ao tentar criar candidatura. Pessoa nao encontrada com id: {$data['id_pessoa']}");
            }
        } else {
            return $this->sendErrorResponse(400, 40003, "Erro ao tentar criar candidatura. Parametro 'id_pessoa' obrigatorio.");
        }

        // Validates job opportunity.
        if (isset($data['id_vaga'])) {
            $JobOpportunityTable = $this->getServiceLocator()->get('Application\Model\JobOpportunityTable');
            $JobOpportunity      = $JobOpportunityTable->get(array('id_job_opportunity' => $data['id_vaga']));
            if (!empty($JobOpportunity) and $JobOpportunity->id_job_opportunity) {
                $Candidature->setJobOpportunity($JobOpportunity);
            } else {
                return $this->sendErrorResponse(400, 40004, "Erro ao tentar criar candidatura. Vaga nao encontrada com id: {$data['id_vaga']}");
            }
        } else {
            return $this->sendErrorResponse(400, 40005, "Erro ao tentar criar candidatura. Parametro 'id_vaga' obrigatorio.");
        }

        $CandidatureTable = $this->getServiceLocator()->get('Application\Model\CandidatureTable');

        // Checks whether the condidate already applied to the job opportunity.
        $ExistingCandidature = $CandidatureTable->get(
            array('id_job_opportunity' => $data['id_vaga'], 'id_candidate' => $data['id_pessoa'])
        );

        if (!empty($ExistingCandidature) and $ExistingCandidature->id_candidate) {
            $this->response->setStatusCode(200); // Ok.
            $response = $this->getResponse()
                ->setContent(
                    json_encode(
                        array(
                            'code'    => 200,
                            'message' => 'Candidato previamente ja cadastrado na vaga.',
                            'id'      => $ExistingCandidature->id_candidature,
                            'href'    => sprintf('%s/%s', self::BASE_URL, $ExistingCandidature->id_candidature),
                        )
                    )
                );
            return $response;
        }

        $candidatureId = $CandidatureTable->save($Candidature);

        $this->response->setStatusCode(201); // Created.
        $response = $this->getResponse()
            ->setContent(
                json_encode(
                    array(
                        'code'    => 201,
                        'message' => 'Candidatura criada com sucesso.',
                        'id'      => $candidatureId,
                        'href'    => sprintf('%s/%s', self::BASE_URL, $candidatureId),
                    )
                )
            );
        return $response;

    }//end create()


}//end class