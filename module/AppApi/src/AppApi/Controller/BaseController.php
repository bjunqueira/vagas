<?php
namespace AppApi\Controller;

use Zend\Mvc\Controller\AbstractRestfulController;


/**
 * Base controller for Apis.
 *
 * @author brunojunqueira
 */
class BaseController extends AbstractRestfulController
{


    /**
     * Builds the error response and sets the response http status code.
     *
     * @return string
     */
    protected function sendErrorResponse($httpCode, $errorCode, $message)
    {
        $this->response->setStatusCode($httpCode);
        $Response = $this->getResponse()
            ->setContent(
                json_encode(
                    array(
                        'code'    => $errorCode,
                        'message' => $message,
                    )
                )
            );
        return $Response;

    }//end sendErrorResponse()


}//end class