<?php
return array(
    'controllers' => array(
        'invokables' => array(
            'AppApi\Controller\v1\Vagas'                      => 'AppApi\Controller\v1\VagasController',
            'AppApi\Controller\v1\Pessoas'                    => 'AppApi\Controller\v1\PessoasController',
            'AppApi\Controller\v1\Candidaturas'               => 'AppApi\Controller\v1\CandidaturasController',
            'AppApi\Controller\v1\Vagas\Candidaturas\Ranking' => 'AppApi\Controller\v1\Vagas\Candidaturas\RankingController',
        ),
    ),
    'router' => array(
        'routes' => array(
            'vagas_ranking' => array(
                'type'    => 'Segment',
                'options' => array(
                    'route'    => '/v1/vagas[/:id_vaga]',
                    'defaults' => array(
                        '__NAMESPACE__' => 'AppApi\Controller\v1',
                        'controller'    => 'Vagas',
                    ),
                ),
                'child_routes' => array(
                    'candidaturas' => array(
                        'type' => 'Literal',
                        'options' => array(
                            'route'      => '/candidaturas/ranking',
                            'controller' => 'Vagas',
                            'defaults' => array(
                                'action' => 'getRanking',
                            ),
                        ),
                    ),
                ),
            ),
            'vagas' => array(
                'type'    => 'Literal',
                'options' => array(
                    'route'    => '/v1/vagas',
                    'defaults' => array(
                        '__NAMESPACE__' => 'AppApi\Controller\v1',
                        'controller'    => 'Vagas',
                    ),
                ),
            ),
            'pessoas' => array(
                'type'    => 'Literal',
                'options' => array(
                    'route'    => '/v1/pessoas',
                    'defaults' => array(
                        '__NAMESPACE__' => 'AppApi\Controller\v1',
                        'controller'    => 'Pessoas',
                    ),
                ),
            ),
            'candidaturas' => array(
                'type'    => 'Literal',
                'options' => array(
                    'route'    => '/v1/candidaturas',
                    'defaults' => array(
                        '__NAMESPACE__' => 'AppApi\Controller\v1',
                        'controller'    => 'Candidaturas',
                    ),
                ),
            ),
        ),
    ),
    'view_manager' => array(
        'strategies' => array(
            'ViewJsonStrategy',
        ),
    ),
);